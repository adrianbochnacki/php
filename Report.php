<?php

class WordTemplateProcessor
{

    private $templatePath;
    private $documentXml;

    public function __construct($templatePath)
    {
        $this->templatePath = $templatePath;
        $this->loadDocumentXml();
    }

    private function loadDocumentXml()
    {
        $zip = new ZipArchive();
        if ($zip->open($this->templatePath) === TRUE) {
            // Read the content of the document.xml file from the Word document
            $documentXmlIndex = $zip->locateName('word/document.xml');
            $this->documentXml = $zip->getFromIndex($documentXmlIndex);
            $zip->close();
        } else {
            throw new Exception('Failed to open template file.');
        }
    }

    public function replacePlaceholders($placeholderArray)
    {
        // Load the document XML into a DOMDocument object
        $dom = new DOMDocument();
        $dom->preserveWhiteSpace = false;
        $dom->loadXML($this->documentXml);

        // Search for all <w:t> elements within the document
        $xpath = new DOMXPath($dom);
        $nodes = $xpath->query('//w:t');

        // Iterate through the nodes and inspect their text content to identify placeholders
        foreach ($nodes as $node) {
            // Extract text content of the node
            $text = $node->textContent;

            // Search for placeholders in the text content
            preg_match_all('/\$[a-zA-Z0-9_]+/', $text, $matches);

            // Iterate through the placeholders and print them
            foreach ($matches[0] as $placeholder) {
                // Print the placeholder found in the document
                echo "Placeholder found in document: $placeholder<br>";
            }
        }

        // Print the placeholders from the $placeholderArray
        echo "Placeholders from placeholderArray:<br>";
        foreach ($placeholderArray as $placeholder => $replace) {
            echo "$placeholder<br>";
        }

        // Perform placeholder replacement
        foreach ($placeholderArray as $placeholder => $replace) {
            // Search for text nodes containing the placeholder
            $nodes = $xpath->query('//w:t[text()="$' . $placeholder . '"]');

            // Iterate through matching nodes and replace the placeholder
            foreach ($nodes as $node) {
                $node->nodeValue = $replace;
            }
        }

        // Save the modified document XML
        $this->documentXml = $dom->saveXML();
    }











    public function saveAs($newTemplatePath)
    {
        $zip = new ZipArchive();
        if ($zip->open($newTemplatePath, ZipArchive::CREATE) === TRUE) {
            $zip->addFromString('word/document.xml', $this->documentXml);
            $zip->close();
        } else {
            throw new Exception('Failed to save modified template file.');
        }
    }

    public function getDocumentXml()
    {
        return $this->documentXml;
    }
}

// Example usage:
$templatePath = "C:\inetpub\wwwroot\PHP\php.git\decorated_quote.docx";
$newTemplatePath = "C:\inetpub\wwwroot\PHP\php.git\decorated_quote_modified.docx";

$wordProcessor = new WordTemplateProcessor($templatePath);
$wordProcessor->replacePlaceholders([
    'ReportNo' => '123',
    'PRef' => '12345'
]);

// Debugging: Check if the documentXml contains the placeholder
echo "Original XML: <pre>" . htmlspecialchars($wordProcessor->getDocumentXml()) . "</pre>";

$wordProcessor->saveAs($newTemplatePath);
