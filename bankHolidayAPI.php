<?php
#2 write some code that queries the API and uses the response to show future bank holidays in each country 

$countries = [];

$api_url = 'https://www.gov.uk/bank-holidays.json';

// Read JSON file
$json_data = file_get_contents($api_url);

// Set up array and decode json
$countriesBankHolidays =[];
$bankHolidays = json_decode($json_data, true);

// Get index countries names from API - We could directly call the names as we know the countries in UK however following task i'm going to use response to get that data
$countries = array_keys($bankHolidays);

$countriesBankHolidays[$countries[0]] =$bankHolidays[$countries[0]]['events']; // england-and-wales
$countriesBankHolidays[$countries[1]] =$bankHolidays[$countries[1]]['events']; // scotland 
$countriesBankHolidays[$countries[2]] =$bankHolidays[$countries[2]]['events']; // northern-ireland
$dateToday = new DateTime();

echo "#2 Bank holiday API Future Data";
echo $lineBreak.$lineBreak ;

/*  england-and-wales  */
echo '<b>'.$countries[0].'</b>'.$lineBreak ;
foreach($countriesBankHolidays[$countries[0]] as $holiday){
$BankHolidayDate = new DateTime( $holiday['date']);
if($BankHolidayDate > $dateToday){ //Check if bank holiday is future date and display
echo $holiday['title'].' - ' .date("d/m/Y", strtotime($holiday['date'])).' '.$holiday['notes'].$lineBreak;
}
}
echo $lineBreak ;
/*  scotland   */
echo '<b>'.$countries[1].'</b>'.$lineBreak ;
foreach($countriesBankHolidays[$countries[1]] as $holiday){
$BankHolidayDate = new DateTime( $holiday['date']);
if($BankHolidayDate > $dateToday){ //Check if bank holiday is future date and display
echo $holiday['title'].' - ' .date("d/m/Y", strtotime($holiday['date'])).' '.$holiday['notes'].$lineBreak;
}
}
echo $lineBreak ;
/*  northern-ireland  */
echo '<b>'.$countries[2].'</b>'.$lineBreak ;
foreach($countriesBankHolidays[$countries[2]] as $holiday){
$BankHolidayDate = new DateTime( $holiday['date']);
if($BankHolidayDate > $dateToday){ //Check if bank holiday is future date and display
echo $holiday['title'].' - ' .date("d/m/Y", strtotime($holiday['date'])).' '.$holiday['notes'].$lineBreak;
}
}
echo $lineBreak ;



?>